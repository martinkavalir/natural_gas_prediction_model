# Natural_gas_prediction_model

Public ilustration of natural gas price model. The purpose of this model is to predict price in short/medium term.
There are 3 main subparts of calculations - weather, supply/demand and price. This ilustration is mainly focused on weather part.
This type of model can be used for price predictions of other commodities such as corn, wheat, cotton, soybeans, sugar, coffee etc.