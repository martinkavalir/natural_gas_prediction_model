"""Cílem tohoto modulu je dělat unit testy"""
import unittest
from datetime import date
from calculations.subsidiary.dates.date_classes import DateOperations


class TestDateOperations(unittest.TestCase):
    """Slouží k otestování tříd a metod ze souboru general_classes.py"""
    def __init__(self, *args, **kwargs):
        """Create instance which will be used in all testes methods"""
        super(TestDateOperations, self).__init__(*args, **kwargs)
        self.date_list = DateOperations(date(2020, 5, 31), date(2020, 6, 2))

    def test_create_date_list(self):
        """Testing of method create_date_list"""
        self.assertEqual(
            self.date_list.create_date_list(),
            [date(2020, 5, 31), date(2020, 6, 1), date(2020, 6, 2)]
        )

    def test_create_month_list(self):
        """Testing of method create_month_list"""
        self.date_list.create_date_list()
        self.assertEqual(self.date_list.create_month_list(), ["05", "06"])

    def test_create_year_month_list(self):
        """Testing of method create_year_month_list"""
        self.date_list.create_date_list()
        self.assertEqual(self.date_list.create_year_month_list(), ["202005", "202006"])


if __name__ == 'main':
    unittest.main()
