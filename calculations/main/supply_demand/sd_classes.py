"""Cílem tohoto modulu je vytvořit metody, které budou pracovat s daty o nabídce/poptávce"""
from sqlalchemy import select
from database_handling.table_definition import SupplyDemandDaily as Sdd
from calculations.subsidiary.general.general_functions import list_from_select


class SupplyDemandData:
    """Obsahuje metody, jak pracovat s daty o nabídce/poptávce"""
    def __init__(self, session=None):
        self.session = session

    def get_computed_sd(self, first_date, last_date):
        """Použije se pro ověření, zda mají dny, které předchází aktuálnímu dni spočtené zásoby"""
        computed_sd = self.session.execute(select(Sdd.from_date)
                                           .where(Sdd.from_date >= first_date)
                                           .where(Sdd.from_date < last_date))
        return list_from_select(computed_sd)
