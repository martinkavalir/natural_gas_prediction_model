"""Contains all classes for getting weather data together."""
import numpy as np
from sqlalchemy import select, func
from database_handling.table_definition import Forecast as Fo, County as Co, TemperatureCurve as Tc
from calculations.subsidiary.general.general_functions import list_from_select, list_reduction
from calculations.subsidiary.general.create_optimal_bst import find_optimal_binary_search_tree, Node
from calculations.subsidiary.general.binary_tree import BinarySearchTree
from calculations.main.weather.temperature_function import temp_func_arg as t_f_c, count_wcdd_whdd


class WeatherForecast:
    """Is for getting current date and prior date forecast data as defined."""
    def __init__(self, session, date_list, state_list, county_list):
        self.session = session
        self.date_list = date_list
        self.state_list = state_list
        self.county_list = county_list
        self.forecast_county_dict = self.__create_county_dictionary()
        self.forecast_state_dict = self.__create_state_dictionary()
        self.optimal_tree = self.__create_optimal_county_bst_list()
        self.county_weight_bst = self.__create_county_weight_structure()

    def __create_county_dictionary(self):
        """Creates needed county dictionary structure."""
        forecast_county_dictionary = {}
        for date in self.date_list:
            forecast_county_dictionary[date] = {}
            for state in self.state_list:
                forecast_county_dictionary[date][state] = {}
                for county in self.county_list:
                    if county[1] == state:
                        forecast_county_dictionary[date][state][county[0]] = None
                    else:
                        continue
        return forecast_county_dictionary

    def __create_state_dictionary(self):
        """Creates needed state dictionary structure."""
        forecast_state_dictionary = {}
        for date in self.date_list:
            forecast_state_dictionary[date] = {}
            for state in self.state_list:
                forecast_state_dictionary[date][state] = None
        return forecast_state_dictionary

    def __create_optimal_county_bst_list(self):
        """Creates list of counties in optimal order for optimal BST."""
        nodes = []
        for county in self.county_list:
            nodes.append(Node(county[0], 1))
        optimal_tree = find_optimal_binary_search_tree(nodes)
        return optimal_tree

    def __create_county_weight_structure(self):
        """Creates BST from county and county_weights"""
        county_weight_tree = BinarySearchTree()
        for c_id in self.optimal_tree:
            c_id_idx = np.where(np.array([i[0:1] for i in self.county_list]).flatten() == c_id)[0]
            county_weight_tree.insert(c_id, self.county_list[c_id_idx[0]][2])
        return county_weight_tree

    def __fill_county_dictionary(self, data_object):
        """Updates data in predefined structure."""
        for row in data_object:
            temp_avg = (row['temp_min'] + row['temp_max']) / 2
            c_weight = self.county_weight_bst.find(row['county_id']).val
            wcdd_whdd = count_wcdd_whdd(temp_avg, c_weight)
            self.forecast_county_dict[row['DATE']][row['state_id']][row['county_id']] = wcdd_whdd

    def get_forecast_data(self, weather_date):
        """Fills current forecast to forecast_dict."""
        forecast_data = self.session.execute(select(
                Fo.county_id, Fo.temp_min, Fo.temp_max, func.DATE(Fo.dt_txt),
                Co.state_id).join(Co, Fo.county_id == Co.id)
                                 .where(Fo.updated_at_date == weather_date)
                                 )
        self.__fill_county_dictionary(forecast_data)

    def get_preforecast_data(self, weather_date):
        """Fills preforecast data to forecast_dict."""
        preforecast_data = self.session.execute(select(
                Fo.county_id, Fo.temp_min, Fo.temp_max, func.DATE(Fo.dt_txt),
                Co.state_id).join(Co, Fo.county_id == Co.id)
                                 .where(Fo.updated_at_date == weather_date)
                                 .where(Fo.updated_at_date == func.DATE(Fo.dt_txt))
                                 )

        self.__fill_county_dictionary(preforecast_data)

    def fill_missing_data(self, session_2, months_to_compute, average_type_year, updated_at):
        """Gets weather coefficients for each county and fills missing data."""
        weather_curve_2d_list = []
        for month_to_compute in months_to_compute:
            weather_c = list_from_select(
                session_2.execute(select(Tc.county_id, Tc.section, Tc.coef_a, Tc.coef_b)
                                  .where(Tc.average_type_year == average_type_year)
                                  .where(Tc.updated_at == updated_at)
                                  .where(Tc.section == month_to_compute)))
            weather_curve_2d_list.append(weather_c)
        weather_curve_list = sorted(list_reduction(weather_curve_2d_list))
        self.__fill_missing_data(self.__create_county_curve_structure(weather_curve_list))

    def __fill_missing_data(self, weather_c):
        """Scans forecast_dict and fills in missing data."""
        for d_key in self.forecast_county_dict:
            for s_key in self.forecast_county_dict[d_key]:
                for c_key in self.forecast_county_dict[d_key][s_key]:
                    if self.forecast_county_dict[d_key][s_key][c_key] is None:
                        county_id_sect = weather_c.find(c_key).val
                        coefs = county_id_sect[d_key.month]
                        c_weight = self.county_weight_bst.find(c_key).val
                        temp_avg = round(coefs[0] * t_f_c(d_key) + coefs[1], 1)
                        wcdd_whdd = count_wcdd_whdd(temp_avg, c_weight)
                        self.forecast_county_dict[d_key][s_key][c_key] = wcdd_whdd
                    else:
                        continue

    def __create_county_curve_structure(self, l_data):
        """Fills curve coefficients to BST."""
        county_curve_tree = BinarySearchTree()
        for county_id in self.optimal_tree:
            curve_dict = {}
            county_id_idxs = np.where(np.array([i[0:1] for i in l_data]).flatten() == county_id)[0]
            for c_id_idx in county_id_idxs:
                curve_dict[l_data[c_id_idx][1]] = [l_data[c_id_idx][2], l_data[c_id_idx][3]]
            county_curve_tree.insert(county_id, curve_dict)
        return county_curve_tree

    def fill_state_dictionary(self):
        """This metod is for hdd/cdd summary"""
        for d_key in self.forecast_county_dict:
            for s_key in self.forecast_county_dict[d_key]:
                wcdd_s = 0
                whdd_s = 0
                for c_key in self.forecast_county_dict[d_key][s_key]:
                    wcdd_s += self.forecast_county_dict[d_key][s_key][c_key][0]
                    whdd_s += self.forecast_county_dict[d_key][s_key][c_key][1]
                    self.forecast_state_dict[d_key][s_key] = [round(wcdd_s, 3), round(whdd_s, 3)]
