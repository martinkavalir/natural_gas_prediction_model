"""In the module are functions associated with weather."""


def temp_func_arg(evaluated_day):
    """Function which creates an argument for temperature function"""
    day_number = evaluated_day.day
    month_number = evaluated_day.month
    if month_number == 1:
        if day_number <= 15:
            return 365 + day_number
        else:
            return day_number
    else:
        return {
            2: 31 + day_number,
            3: 59 + day_number,
            4: 90 + day_number,
            5: 120 + day_number,
            6: 151 + day_number,
            7: 181 + day_number,
            8: 212 + day_number,
            9: 243 + day_number,
            10: 273 + day_number,
            11: 304 + day_number,
        }.get(month_number, 334 + day_number)


def count_wcdd_whdd(avg_temp, c_weight):
    """Function which count number of weighted cdd/hdd."""
    if avg_temp > 65:
        w_hdd = 0
        w_cdd = (avg_temp - 65) * c_weight
    elif avg_temp < 65:
        w_hdd = (65 - avg_temp) * c_weight
        w_cdd = 0
    else:
        w_hdd = 0
        w_cdd = 0
    return [round(w_cdd, 5), round(w_hdd, 5)]
