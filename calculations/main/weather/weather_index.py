"""
Purpose of this modulu is to follow steps which are necessary to transform county level data
to state level data.
"""
import config as cfg
import database_handling.db_sessions as db_sessions
import calculations.subsidiary.dates.date_data as date_data
import calculations.subsidiary.geography.geo_data as geo_data
from calculations.main.weather.weather_classes import WeatherForecast


def compute_weather_data(current_date):
    weather_data = WeatherForecast(db_sessions.SESSION_3, date_data.COMPLETE_DATE_LIST,
                                   geo_data.STATES, geo_data.COUNTY_WEIGHTS)
    weather_data.get_forecast_data(current_date)
    for pre_date in date_data.missing_dates_pf:
        weather_data.get_preforecast_data(pre_date)
    weather_data.fill_missing_data(db_sessions.SESSION_2, date_data.MONTHS, cfg.AVERAGE_TYPE_YEAR,
                                   cfg.UPDATED_AT)

    weather_data.fill_state_dictionary()
