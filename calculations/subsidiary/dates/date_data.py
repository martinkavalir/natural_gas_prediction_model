"""Module where are all needed date data created. """
import datetime as dt
from sqlalchemy import select, asc, desc
from dateutil.relativedelta import relativedelta
import config
import database_handling.db_sessions as db_sessions
from calculations.subsidiary.dates.date_classes import DateOperations as D_o
from calculations.subsidiary.general.general_functions import list_from_select
from database_handling.table_definition import FuturesCalendar as Fc, SupplyDemand as Sd
from calculations.subsidiary.general.general_functions import compare_two_lists, merge_two_lists
from calculations.main.supply_demand.sd_classes import SupplyDemandData

FUTURES_DATES = list_from_select(
    db_sessions.SESSION_3.execute(select(Fc.settlement)
                                  .where(Fc.settlement >= config.start_date)
                                  .where(Fc.asset_id == 7)
                                  .order_by(asc(Fc.settlement))
                                  .limit(2))
)

date_list = D_o(config.start_date, FUTURES_DATES[len(FUTURES_DATES) - 1][0])
date_list.create_date_list()
DATES = date_list.final_date_list
MONTHS = date_list.create_month_list()
YEAR_MONTHS = date_list.create_year_month_list()

LAST_SD_DATE = list_from_select(
    db_sessions.SESSION_3.execute
    (select(Sd.from_date, Sd.sd_value)
     .where(Sd.update_horizont == 'weekly')
     .where(Sd.category_type == 'working_gas_storage')
     .where(Sd.from_date >= int((config.start_date + relativedelta(days=-14)).strftime("%Y%m%d")))
     .where(Sd.from_date < int((config.start_date + relativedelta(days=-6)).strftime("%Y%m%d")))
     .order_by(desc(Sd.from_date))
     .limit(1))
)

LAST_SD_DATA_PLUS_1 = dt.datetime.strptime(str(LAST_SD_DATE[0][0]), '%Y%m%d').date() \
                      + relativedelta(days=1)

PREVIOUS_DATES = D_o(LAST_SD_DATA_PLUS_1, config.start_date - relativedelta(days=1))\
    .create_date_list()

supply_demand_s1 = SupplyDemandData(session=db_sessions.SESSION_1)
computed_storage = supply_demand_s1.get_computed_sd(LAST_SD_DATA_PLUS_1, config.start_date)
# Dates prior current date for which program will get data from forecast table
missing_dates_pf = compare_two_lists(PREVIOUS_DATES, computed_storage)

COMPLETE_DATE_LIST = merge_two_lists(DATES, missing_dates_pf)
