"""Agregate classes which will be used for work with dates."""
import datetime
import config


class DateOperations:
    """ Purpose is to get all needed dates for model computation at specific time."""
    def __init__(self, first_date: datetime.date = None, second_date: datetime.date = None):
        self.first_date = first_date
        self.second_date = second_date
        self.final_date_list = []

    def create_date_list(self):
        """Create list of dates between 2 dates."""
        while self.first_date <= self.second_date:
            self.final_date_list.append(self.first_date)
            self.first_date += config.DELTA

        return self.final_date_list

    def create_month_list(self):
        """Creates list of unique months in string format which are in date_list."""
        month_list = [self.final_date_list[0].strftime("%m")]
        for i in range(1, len(self.final_date_list)):
            if self.final_date_list[i].strftime("%m") == month_list[len(month_list) - 1]:
                continue
            else:
                month_list.append(self.final_date_list[i].strftime("%m"))
        return month_list

    def create_year_month_list(self):
        """Creates list of unique months in string format 'yyyymm' which are in date_list."""
        uni_y_m_list = [self.final_date_list[0].strftime("%Y%m")]
        for i in range(1, len(self.final_date_list)):
            if self.final_date_list[i].strftime("%Y%m") == uni_y_m_list[len(uni_y_m_list) - 1]:
                continue
            else:
                uni_y_m_list.append(self.final_date_list[i].strftime("%Y%m"))

        return uni_y_m_list
