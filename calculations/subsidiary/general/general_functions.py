"""This module contains basic functions."""
import numpy as np


def list_from_select(data_object):
    """Function takes sqlalchemy.engine.result.ChunkedIteratorResult and creates list from it."""
    final_list = []
    data_keys = data_object.keys()
    for row in data_object:
        wip_list = []
        for data_key in data_keys:
            wip_list.append(row[data_key])
        final_list.append(wip_list)
    return final_list


def compare_two_lists(list_1, list_2):
    """
    Function compare two lists of unique data and returns list of values which are in list 1 but
    not in list 2.
    """
    final_list = []
    for val in list_1:
        val_idx = np.where(np.array(list_2) == val)[0]
        if len(val_idx) == 0:
            final_list.append(val)
        else:
            continue
    return final_list


def merge_two_lists(list_1, list_2):
    """Function is for merging two lists [] + [] = []."""
    final_list = []
    lists = [list_1, list_2]
    for s_list in lists:
        for val in s_list:
            final_list.append(val)
    return final_list


def list_reduction(list_to_reduce):
    """Function is for reducing 2D list to 1D."""
    new_list = []
    for i in list_to_reduce:
        for j in i:
            new_list.append(j)
    return new_list
