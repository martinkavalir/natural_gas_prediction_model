"""
Modified algorithm from:
http://python.algorithmexamples.com/web/dynamic_programming/optimal_binary_search_tree.html
"""
import sys
FINAL_BST = []


class Node:

    def __init__(self, key, freq):
        self.key = key
        self.freq = freq


def create_binary_search_tree(root, key, i, j):

    if i > j or i < 0 or j > len(root) - 1:
        return
    node = root[i][j]
    FINAL_BST.append(key[node])
    create_binary_search_tree(root, key, i, node - 1)
    create_binary_search_tree(root, key, node + 1, j)


def find_optimal_binary_search_tree(nodes):
    nodes.sort(key=lambda node: node.key)

    no_of_nodes = len(nodes)

    keys = [nodes[i].key for i in range(no_of_nodes)]
    freqs = [nodes[i].freq for i in range(no_of_nodes)]

    # This 2D array stores the overall tree cost (which's as minimized as possible);
    # for a single key, cost is equal to frequency of the key.
    d_p = [[freqs[i] if i == j else 0 for j in range(no_of_nodes)] for i in range(no_of_nodes)]
    # sum_it[i][j] stores the sum of key frequencies between i and j inclusive in nodes
    # array
    sum_it = [[freqs[i] if i == j else 0 for j in range(no_of_nodes)] for i in range(no_of_nodes)]
    # stores tree roots that will be used later for constructing binary search tree
    root = [[i if i == j else 0 for j in range(no_of_nodes)] for i in range(no_of_nodes)]

    for interval_length in range(2, no_of_nodes + 1):
        for i in range(no_of_nodes - interval_length + 1):
            j = i + interval_length - 1

            d_p[i][j] = sys.maxsize  # set the value to "infinity"
            sum_it[i][j] = sum_it[i][j - 1] + freqs[j]

            # Apply Knuth's optimization
            # Loop without optimization: for r in range(i, j + 1):
            for r_no in range(root[i][j - 1], root[i + 1][j] + 1):  # r_no is a temporal root
                left = d_p[i][r_no - 1] if r_no != i else 0  # optimal cost for left subtree
                right = d_p[r_no + 1][j] if r_no != j else 0  # optimal cost for right subtree
                cost = left + sum_it[i][j] + right

                if d_p[i][j] > cost:
                    d_p[i][j] = cost
                    root[i][j] = r_no
            opt_bst_cost = d_p[0][no_of_nodes - 1]
    print(f"\nThe cost of optimal BST for given tree nodes is {opt_bst_cost}.")

    create_binary_search_tree(root, keys, 0, no_of_nodes - 1)
    return FINAL_BST
