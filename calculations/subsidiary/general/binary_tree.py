"""
Modified algorithm from:
https://github.com/bfaure/Python3_Data_Structures/blob/master/Binary_Search_Tree/main.py
"""


class Node:
    def __init__(self, key=None, val=None):
        """ Inicializace uzlu """
        self.key = key
        self.val = val
        self.left_child = None
        self.right_child = None
        self.parent = None


class BinarySearchTree:
    def __init__(self):
        self.root = None

    def insert(self, key, val):
        """Metoda slouží k vložení hodnoty bst"""
        if self.root is None:
            self.root = Node(key, val)
        else:
            self._insert(key, val, self.root)

    def _insert(self, key, val, cur_node):
        """ Privátní metoda - tz. lze k ní přistupovat pouze v rámci dané třídy. Rekurze."""
        if key < cur_node.key:
            if cur_node.left_child is None:
                cur_node.left_child = Node(key, val)
                cur_node.left_child.parent = cur_node  # set parent
            else:
                self._insert(key, val, cur_node.left_child)
        elif key > cur_node.key:
            if cur_node.right_child is None:
                cur_node.right_child = Node(key, val)
                cur_node.right_child.parent = cur_node  # set parent
            else:
                self._insert(key, val, cur_node.right_child)
        else:
            print("key already in tree!")

    def print_tree(self):
        """Slouží čistě jen k zobrazení stromu"""
        if self.root is not None:
            self._print_tree(self.root)

    def _print_tree(self, cur_node):
        """Privátní metoda k předchozí kde je logika."""
        if cur_node is not None:
            self._print_tree(cur_node.left_child)
            print(str(cur_node.key), cur_node.val)
            self._print_tree(cur_node.right_child)

    def height(self):
        """Slouží k určení výšky stromu"""
        if self.root is not None:
            return self._height(self.root, 0)
        else:
            return 0

    def _height(self, cur_node, cur_height):
        """Privátní metoda pro logiku metody height."""
        if cur_node is None:
            return cur_height
        left_height = self._height(cur_node.left_child, cur_height + 1)
        right_height = self._height(cur_node.right_child, cur_height + 1)
        return max(left_height, right_height)

    def find(self, key):
        """Slouží k nalezení určité hodnoty, lze pomocí ní vrátit objekt vztažený k danému uzlu."""
        if self.root is not None:
            return self._find(key, self.root)
        else:
            return None

    def _find(self, key, cur_node):
        """Privátní metoda k metodě find. Vyhledání pouze na základě 'key' """
        if key == cur_node.key:
            return cur_node
        elif key < cur_node.key and cur_node.left_child is not None:
            return self._find(key, cur_node.left_child)
        elif key > cur_node.key and cur_node.right_child is not None:
            return self._find(key, cur_node.right_child)

    def delete_key(self, key):
        """Slouží ke smazání hodnoty."""
        return self.delete_node(self.find(key))

    def delete_node(self, node):
        """Slouží ke smazání uzlu."""

        # Protect against deleting a node not found in the tree
        if node is None or self.find(node.key) is None:
            print("Node to be deleted not found in the tree!")
            return None

        # returns the node with min key in tree rooted at input node
        def min_key_node(n):
            current = n
            while current.left_child is not None:
                current = current.left_child
            return current

        # returns the number of children for the specified node
        def num_children(n):
            nums_children = 0
            if n.left_child is not None:
                nums_children += 1
            if n.right_child is not None:
                nums_children += 1
            return nums_children

        # get the parent of the node to be deleted
        node_parent = node.parent

        # get the number of children of the node to be deleted
        node_children = num_children(node)

        # break operation into different cases based on the
        # structure of the tree & node to be deleted

        # CASE 1 (node has no children)
        if node_children == 0:

            # Added this if statement post-video, previously if you
            # deleted the root node it would delete entire tree.
            if node_parent is not None:
                # remove reference to the node from the parent
                if node_parent.left_child == node:
                    node_parent.left_child = None
                else:
                    node_parent.right_child = None
            else:
                self.root = None

        # CASE 2 (node has a single child)
        if node_children == 1:

            # get the single child node
            if node.left_child is not None:
                child = node.left_child
            else:
                child = node.right_child

            # Added this if statement post-video, previously if you
            # deleted the root node it would delete entire tree.
            if node_parent is not None:
                # replace the node to be deleted with its child
                if node_parent.left_child == node:
                    node_parent.left_child = child
                else:
                    node_parent.right_child = child
            else:
                self.root = child

            # correct the parent pointer in node
            child.parent = node_parent

        # CASE 3 (node has two children)
        if node_children == 2:
            # get the inorder successor of the deleted node
            successor = min_key_node(node.right_child)

            # copy the inorder successor's key to the node formerly
            # holding the key we wished to delete
            node.key = successor.key

            # delete the inorder successor now that it's key was
            # copied into the other node
            self.delete_node(successor)

    def search(self, key):
        """Vrací true/false v případech, kdy hodnota je ve stromu resp. není"""
        if self.root is not None:
            return self._search(key, self.root)
        else:
            return False

    def _search(self, key, cur_node):
        """Privátní funkce k funkci search"""
        if key == cur_node.key:
            return True
        elif key < cur_node.key and cur_node.left_child is not None:
            return self._search(key, cur_node.left_child)
        elif key > cur_node.key and cur_node.right_child is not None:
            return self._search(key, cur_node.right_child)
        return False
