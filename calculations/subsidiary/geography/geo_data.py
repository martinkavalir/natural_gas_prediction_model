"""V tomto modulu budu pracovat s daty jako county, state atp."""
import numpy as np
import pandas as pd
from sqlalchemy import select
import database_handling.db_sessions as db_sessions
from calculations.subsidiary.general.general_functions import list_from_select

from database_handling.table_definition import County as Co, State as St

COUNTY = list_from_select(db_sessions.SESSION_3.execute(
    select(Co.id, Co.state_id, Co.population, Co.area))
)

STATES = [i[:1][0] for i in list_from_select(db_sessions.SESSION_3.execute(
    select(St.code).where(St.country_code == 'US').where(St.code != 'AK').where(St.code != 'HI')))]


DF1 = pd.DataFrame({'state': np.array([i[1:2] for i in COUNTY]).flatten(),
                    'population': np.array([i[2:3] for i in COUNTY]).flatten()})
STATE_POPULATION = DF1.groupby(['state']).sum()
COUNTY_WEIGHTS = []
STATE_ARRAY = []
for index, row in STATE_POPULATION.iterrows():
    STATE_ARRAY.append([index, row['population']])
for idx, val in enumerate(COUNTY):
    state_index = np.where(np.array([i[:1] for i in STATE_ARRAY]).flatten() == val[1])[0]
    COUNTY_WEIGHTS.append([val[0], val[1], val[2] / STATE_ARRAY[state_index[0]][1]])

