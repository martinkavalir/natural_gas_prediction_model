""" Repository class which contains all needed db entities with attributes"""
from sqlalchemy.orm import declarative_base
from sqlalchemy import MetaData
from sqlalchemy import Column, String, Integer, Float, DateTime, BINARY, Date,\
    Enum

METADATA = MetaData()
BASE = declarative_base()


class CalculationStatus(BASE):
    __tablename__ = 'storage_calculation_status'
    id = Column("id", Integer, primary_key=True)
    from_date = Column("from_date", DateTime)
    computed_date = Column("computed_date", DateTime)
    rice_regresssion = Column("rice_regresssion_c", BINARY)
    weather = Column("weather_calc", BINARY)
    storage_range = Column("storage_range_calc_status", BINARY)


class FuturesCalendar(BASE):
    __tablename__ = 'futures_calendar'
    id = Column("id", Integer, primary_key=True)
    asset_id = Column("asset_id", Integer)
    futures_month = Column("futures_month", Integer)
    settlement = Column("settlement", Date)
    rollover_date = Column("rollover_date", Date)


class Forecast(BASE):
    __tablename__ = 'forecast'
    id = Column("id", Integer, primary_key=True)
    county_id = Column("county_id", Integer)
    temp_min = Column("temp_min", Integer)
    temp_max = Column("temp_max", Integer)
    rain = Column("rain", Float)
    humidity = Column("humidity", Integer)
    wind_speed = Column("wind_speed", Float)
    cloud = Column("cloud", Integer)
    dt_txt = Column("dt_txt", DateTime)
    dt = Column("dt", Integer)
    updated_at_date = Column("updated_at_date", Date)
    updated_at = Column("updated_at", DateTime)


class County(BASE):
    __tablename__ = 'county'
    id = Column("id", Integer, primary_key=True)
    open_weather_geoname_id = Column("open_weather_geoname_id", Integer)
    name = Column("name", String)
    open_weather_place_id = Column("open_weather_place_id", Integer)
    place_name = Column("place_name", String)
    lon = Column("lon", Float)
    lat = Column("lat", Float)
    state_id = Column("state_id", String)
    area = Column("area", Float)
    population = Column("population", Integer)
    fips = Column("fips", Integer)


class TemperatureCurve(BASE):
    __tablename__ = 'temperature_curve'
    id = Column("id", Integer, primary_key=True)
    county_id = Column("county_id", Integer)
    section = Column("section", Integer)
    coef_a = Column("coef_a", Float)
    coef_b = Column("coef_b", Float)
    average_type_year = Column("average_type_year", Integer)
    updated_at = Column("updated_at", Integer)


class SupplyDemand(BASE):
    __tablename__ = 'ng_supply_demand'
    id = Column("id", Integer, primary_key=True)
    from_date = Column("from_date", Integer)
    sd_value = Column("sd_value", Integer)
    category = Column("category", String)
    category_type = Column("category_type", String)
    update_horizont = Column("update_horizont", String)
    region = Column("region", String)
    measure_unit = Column("measure_unit", String)


class ComputedWeather(BASE):
    __tablename__ = 'ng_computed_weather'
    id = Column("id", Integer, primary_key=True)
    from_date = Column("from_date", Date)
    computed_date = Column("computed_date", Date)
    state_code = Column("state_code", String)
    w_hdd = Column("w_hdd", Float)
    w_cdd = Column("w_cdd", Float)


class State(BASE):
    __tablename__ = 'state'
    code = Column("code", String, primary_key=True)
    name = Column("name", String)
    country_code = Column("country_code", String)
    fips = Column("fips", Integer)


class WeatherHistoricalData(BASE):
    __tablename__ = 'weather_historical_data'
    id = Column("id", Integer, primary_key=True)
    from_date = Column("from_date", Integer)
    value = Column("value", Float)
    county_fips = Column("county_fips", Integer)
    weather_type = Column("weather_type", String)
    units = Column("units", String)


class MetaTraderPrice(BASE):
    __tablename__ = 'meta_trader_price'
    asset_name = Column("asset_name", Integer, primary_key=True)
    time_type = Column("time_type", Integer, primary_key=True)
    unix_timestamp = Column("unix_timestamp", Integer, primary_key=True)
    open_price = Column("open", Integer)
    high_price = Column("high", Integer)
    low_price = Column("low", Integer)
    close_price = Column("close", Integer)
    volume = Column("volume", Integer)
    spread = Column("spread", Integer)
    broker = Column("broker", Integer)
    coefficient_evaluation = Column("coefficient_evaluation", Integer)
    utility_function = Column("utility_function", Integer)


class ManuallyEnteredNews(BASE):
    __tablename__ = 'manually_entered_news'
    id = Column("id", Integer, primary_key=True)
    asset_id = Column("asset_id", Integer)
    from_date = Column("from_date", Date)
    category_type = Column("category_type", String)
    start_date = Column("start_date", Date)
    end_date = Column("end_date", Date)
    sd_value = Column("sd_value", Float)
    measure_unit = Column("measure_unit", Enum('bcfd', 'mcfd', 'bcf', 'mcf',
                                               'unit'))
    source_link = Column("source_link", String)
    priority = Column("priority", Integer)


class SupplyDemandDaily(BASE):
    __tablename__ = 'ng_supply_demand_daily'
    id = Column("id", Integer, primary_key=True)
    from_date = Column("from_date", Date)
    category_type = Column("category_type", String)
    sd_value = Column("sd_value", Float)
    measure_unit = Column("measure_unit", Enum('bcf'))


class SdForModel(BASE):
    __tablename__ = 'ng_sd_for_model'
    id = Column("id", Integer, primary_key=True)
    from_date = Column("from_date", Date)
    category_type = Column("category_type", String)
    computed_date = Column("computed_date", Date)
    sd_value = Column("sd_value", Float)


class EconomicData(BASE):
    __tablename__ = 'economic_data'
    id = Column("id", Integer, primary_key=True)
    indicator_name = Column("indicator_name", String)
    from_date = Column("from_date", Date)
    indicator_value = Column("indicator_value", Float)
    time_frame = Column("time_frame", String)
    region = Column("region", String)


class RiceCoefOptimization(BASE):
    __tablename__ = 'rice_coef_optimization'
    id = Column("id", Integer, primary_key=True)
    from_date = Column("from_date", Date)
    category_type = Column("category_type", String)
    computed_date = Column("computed_date", Date)
    sd_value = Column("sd_value", Float)
    regression_coef_limit = Column("regression_coef_limit", Float)


class RiceOptimalCoef(BASE):
    __tablename__ = 'rice_optimal_coef'
    for_month = Column("for_month", String, primary_key=True)
    category_type = Column("category_type", String, primary_key=True)
    coefficient = Column("coefficient", Float)
    accuracy = Column("accuracy", Float)
    base_values = Column("base_values", Integer)


class DayTrading(BASE):
    __tablename__ = 'day_trading'
    id = Column(Integer, primary_key=True)
    position_type = Column(String)
    open_time = Column(DateTime)
    close_time = Column(DateTime)
    open_price = Column(Float)
    close_price = Column(Float)
    profit = Column(Float)
    time_period = Column(Integer)
    deviation = Column(Float)
