"""Contains configuration for databases."""
import configparser
import os
from sqlalchemy import engine_from_config
from sqlalchemy.orm import sessionmaker


class DatabaseOperation:
    """Class which contains methods to connect to Database."""
    def __init__(self, database_name=None):
        self.database_name = database_name

    def get_mysql_config_data(self):
        """Gets configuration data for connection from db_conf.conf file."""
        parser = configparser.ConfigParser()
        parser.read(os.path.dirname(os.path.realpath(__file__)) + '/db_conf.conf')
        config = {
            'db.url': 'mysql+pymysql://' +
                      parser.get(self.database_name, "user") + ':' +
                      parser.get(self.database_name, "password") + '@' +
                      parser.get(self.database_name, "host") + ':' +
                      parser.get(self.database_name, "port") + '/' +
                      parser.get(self.database_name, "database"),
            'db.echo': 'True'

                }
        return config

    def __create_mysql_engine(self):
        """Private method to handle mysql engine."""
        return engine_from_config(self.get_mysql_config_data(), prefix="db.")

    def create_mysql_session(self):
        """Public method to get mysql session."""
        return sessionmaker(bind=self.__create_mysql_engine())()
