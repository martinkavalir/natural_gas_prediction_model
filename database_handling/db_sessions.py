"""Creates needed sessions"""
from database_handling.configuration_setup import DatabaseOperation

SESSION_1 = DatabaseOperation('db_natural_gas_model').create_mysql_session()
SESSION_2 = DatabaseOperation('db_computed_data').create_mysql_session()
SESSION_3 = DatabaseOperation('db_raw_data').create_mysql_session()
