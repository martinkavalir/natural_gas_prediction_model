"""
Main file.
There are 3 main subsections - weather, supply demand and price computations.
"""
import config
from calculations.main.weather.weather_index import compute_weather_data

while config.start_date <= config.END_DATE:
    compute_weather_data(config.start_date)

    config.start_date += config.DELTA
