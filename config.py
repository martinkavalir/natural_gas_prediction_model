"""This file contains basic values."""
import datetime

DAYS_IN_FORECAST = 16
COUNTY_NUMBER = 3071
CPU_CORES = 4
YEAR_INTERVAL = 200700
AVERAGE_TYPE_YEAR = 5
UPDATED_AT = 2020                           # last update of temperature curve
start_date = datetime.date(2020, 7, 14)
END_DATE = datetime.date.today()
DELTA = datetime.timedelta(days=1)
